package com.gva.vcpoupa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VcpoupaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VcpoupaApplication.class, args);
	}

}
