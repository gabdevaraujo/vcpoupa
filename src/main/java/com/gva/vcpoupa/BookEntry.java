package com.gva.vcpoupa;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.gva.vcpoupa.enums.BookEntryType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class BookEntry {

    @EqualsAndHashCode.Include
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter @Setter 
    private String name;

    @Getter @Setter 
    private LocalDateTime date;

    @Getter @Setter 
    private BookEntryType bookEntryType;

    @Getter @Setter 
    private boolean isInstallment;

    @Getter @Setter 
    private Double bookValue;    
    
}