package com.gva.vcpoupa.enums;

public enum BookEntryType {
    
    ENTRADA, SAIDA
}